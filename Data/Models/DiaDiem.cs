﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class DiaDiem
{
    public string Id { get; set; } = null!;

    public string? Ten { get; set; }

    public string? LoaiTourId { get; set; }

    public virtual LoaiTour? LoaiTour { get; set; }

    public virtual ICollection<Tour> TourDiemDenNavigations { get; } = new List<Tour>();

    public virtual ICollection<Tour> TourDiemKhoiHanhNavigations { get; } = new List<Tour>();
}
