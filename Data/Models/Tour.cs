﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class Tour
{
    public int Id { get; set; }

    public string? Ten { get; set; }

    public decimal? Gia { get; set; }

    public int? SoLuong { get; set; }

    public string? MoTa { get; set; }

    public string? Hinh { get; set; }

    public string? ThoiGian { get; set; }

    public DateTime? NgayKhoiHanh { get; set; }

    public string? DiemKhoiHanh { get; set; }

    public string? DiemDen { get; set; }

    public string? LoaiTourId { get; set; }

    public virtual ICollection<BinhLuan> BinhLuans { get; } = new List<BinhLuan>();

    public virtual ICollection<DatTour> DatTours { get; } = new List<DatTour>();

    public virtual DiaDiem? DiemDenNavigation { get; set; }

    public virtual DiaDiem? DiemKhoiHanhNavigation { get; set; }

    public virtual LoaiTour? LoaiTour { get; set; }
}
