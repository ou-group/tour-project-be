﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Req
{
    public class TourReq
    {
        public int Id { get; set; }

        public string? Ten { get; set; }

        public decimal? Gia { get; set; }

        public int? SoLuong { get; set; }

        public string? MoTa { get; set; }

        public string? Hinh { get; set; }

        public string? ThoiGian { get; set; }

        public DateTime? NgayKhoiHanh { get; set; }

        public string? DiemKhoiHanh { get; set; }

        public string? DiemDen { get; set; }

        public string? LoaiTourId { get; set; }
    }
}
