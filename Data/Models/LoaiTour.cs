﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class LoaiTour
{
    public string Id { get; set; } = null!;

    public string? Ten { get; set; }

    public virtual ICollection<DiaDiem> DiaDiems { get; } = new List<DiaDiem>();

    public virtual ICollection<Tour> Tours { get; } = new List<Tour>();
}
