﻿using Common.BLL;
using Common.Req;
using Common.Rsp;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TourSvc:GenericSvc<TourRep, Tour>
    {
        public override SingleRsp Read(string key)
        {
            var res = new SingleRsp();
            res.Data = _rep.Read(key);
            return res;
        }
        public override SingleRsp Delete(int id)
        {
            var res = new SingleRsp();
            res.Data = _rep.Delete(id);
            return res;
        }

        public SingleRsp Update(TourReq tourReq)
        {
            var res = new SingleRsp();
            Tour tour = new Tour();
            tour.Id = tourReq.Id;
            tour.Ten = tourReq.Ten;
            tour.Gia = tourReq.Gia;
            tour.MoTa = tourReq.MoTa;
            tour.SoLuong = tourReq.SoLuong;
            tour.Hinh = tourReq.Hinh;
            tour.ThoiGian = tourReq.ThoiGian;
            tour.NgayKhoiHanh = tourReq.NgayKhoiHanh;
            tour.DiemKhoiHanh = tourReq.DiemKhoiHanh;
            tour.DiemDen = tourReq.DiemDen;
            tour.LoaiTourId = tourReq.LoaiTourId;
            res = _rep.Update(tour);
            return res;
        }

        public SingleRsp Insert(TourReq tourReq)
        {
            var res = new SingleRsp();
            Tour tour = new Tour();
            tour.Ten = tourReq.Ten;
            tour.Gia = tourReq.Gia;
            tour.MoTa = tourReq.MoTa;
            tour.SoLuong = tourReq.SoLuong;
            tour.Hinh = tourReq.Hinh;
            tour.ThoiGian = tourReq.ThoiGian;
            tour.NgayKhoiHanh = tourReq.NgayKhoiHanh;
            tour.DiemKhoiHanh = tourReq.DiemKhoiHanh;
            tour.DiemDen = tourReq.DiemDen;
            tour.LoaiTourId = tourReq.LoaiTourId;
            res = _rep.Insert(tour);
            return res;
        }
    }
}
