﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class BinhLuan
{
    public int Id { get; set; }

    public string? BinhLuan1 { get; set; }

    public DateTime? NgayBinhLuan { get; set; }

    public int? NguoiDungId { get; set; }

    public int? TourId { get; set; }

    public virtual NguoiDung? NguoiDung { get; set; }

    public virtual Tour? Tour { get; set; }
}
