﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DAL.Models;

public partial class QuanLyTourContext : DbContext
{
    public QuanLyTourContext()
    {
    }

    public QuanLyTourContext(DbContextOptions<QuanLyTourContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BinhLuan> BinhLuans { get; set; }

    public virtual DbSet<DatTour> DatTours { get; set; }

    public virtual DbSet<DiaDiem> DiaDiems { get; set; }

    public virtual DbSet<LoaiTour> LoaiTours { get; set; }

    public virtual DbSet<NguoiDung> NguoiDungs { get; set; }

    public virtual DbSet<Tour> Tours { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=ADMIN\\SQL2017;Initial Catalog=QuanLyTour;User = steve;Password = 123456;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BinhLuan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__BinhLuan__3214EC27AD2CE5C9");

            entity.ToTable("BinhLuan");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.BinhLuan1).HasColumnName("BinhLuan");
            entity.Property(e => e.NgayBinhLuan).HasColumnType("date");
            entity.Property(e => e.NguoiDungId).HasColumnName("NguoiDungID");
            entity.Property(e => e.TourId).HasColumnName("TourID");

            entity.HasOne(d => d.NguoiDung).WithMany(p => p.BinhLuans)
                .HasForeignKey(d => d.NguoiDungId)
                .HasConstraintName("FK__BinhLuan__NguoiD__693CA210");

            entity.HasOne(d => d.Tour).WithMany(p => p.BinhLuans)
                .HasForeignKey(d => d.TourId)
                .HasConstraintName("FK__BinhLuan__TourID__6A30C649");
        });

        modelBuilder.Entity<DatTour>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__DatTour__3214EC27AFFDCA29");

            entity.ToTable("DatTour");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.NgayDat).HasColumnType("date");
            entity.Property(e => e.NguoiDungId).HasColumnName("NguoiDungID");
            entity.Property(e => e.TourId).HasColumnName("TourID");

            entity.HasOne(d => d.NguoiDung).WithMany(p => p.DatTours)
                .HasForeignKey(d => d.NguoiDungId)
                .HasConstraintName("FK__DatTour__NguoiDu__656C112C");

            entity.HasOne(d => d.Tour).WithMany(p => p.DatTours)
                .HasForeignKey(d => d.TourId)
                .HasConstraintName("FK__DatTour__TourID__66603565");
        });

        modelBuilder.Entity<DiaDiem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__DiaDiem__3214EC274B5F7079");

            entity.ToTable("DiaDiem");

            entity.Property(e => e.Id)
                .HasMaxLength(50)
                .HasColumnName("ID");
            entity.Property(e => e.LoaiTourId)
                .HasMaxLength(50)
                .HasColumnName("LoaiTourID");
            entity.Property(e => e.Ten).HasMaxLength(200);

            entity.HasOne(d => d.LoaiTour).WithMany(p => p.DiaDiems)
                .HasForeignKey(d => d.LoaiTourId)
                .HasConstraintName("FK__DiaDiem__LoaiTou__5BE2A6F2");
        });

        modelBuilder.Entity<LoaiTour>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__LoaiTour__3214EC27E381EA44");

            entity.ToTable("LoaiTour");

            entity.Property(e => e.Id)
                .HasMaxLength(50)
                .HasColumnName("ID");
            entity.Property(e => e.Ten).HasMaxLength(200);
        });

        modelBuilder.Entity<NguoiDung>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__NguoiDun__3214EC276F68FCDE");

            entity.ToTable("NguoiDung");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.DiaChi).HasMaxLength(200);
            entity.Property(e => e.Email).HasMaxLength(200);
            entity.Property(e => e.Sdt)
                .HasMaxLength(50)
                .HasColumnName("SDT");
            entity.Property(e => e.TaiKhoan).HasMaxLength(200);
            entity.Property(e => e.Ten).HasMaxLength(200);
        });

        modelBuilder.Entity<Tour>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Tour__3214EC27262D8790");

            entity.ToTable("Tour");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.DiemDen).HasMaxLength(50);
            entity.Property(e => e.DiemKhoiHanh).HasMaxLength(50);
            entity.Property(e => e.Gia).HasColumnType("money");
            entity.Property(e => e.LoaiTourId)
                .HasMaxLength(50)
                .HasColumnName("LoaiTourID");
            entity.Property(e => e.NgayKhoiHanh).HasColumnType("date");
            entity.Property(e => e.Ten).HasMaxLength(200);
            entity.Property(e => e.ThoiGian).HasMaxLength(100);

            entity.HasOne(d => d.DiemDenNavigation).WithMany(p => p.TourDiemDenNavigations)
                .HasForeignKey(d => d.DiemDen)
                .HasConstraintName("FK__Tour__DiemDen__5FB337D6");

            entity.HasOne(d => d.DiemKhoiHanhNavigation).WithMany(p => p.TourDiemKhoiHanhNavigations)
                .HasForeignKey(d => d.DiemKhoiHanh)
                .HasConstraintName("FK__Tour__DiemKhoiHa__60A75C0F");

            entity.HasOne(d => d.LoaiTour).WithMany(p => p.Tours)
                .HasForeignKey(d => d.LoaiTourId)
                .HasConstraintName("FK__Tour__LoaiTourID__5EBF139D");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
