﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class DatTour
{
    public int Id { get; set; }

    public DateTime? NgayDat { get; set; }

    public int? SoLuong { get; set; }

    public int? NguoiDungId { get; set; }

    public int? TourId { get; set; }

    public virtual NguoiDung? NguoiDung { get; set; }

    public virtual Tour? Tour { get; set; }
}
