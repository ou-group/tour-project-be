﻿using BLL;
using Common.Req;
using Common.Rsp;
using DAL;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class TourController : Controller
    {
        private TourSvc tourSvc;

        public TourController()
        {
            tourSvc = new TourSvc();
        }

        [HttpPost("Get-LoaiTour")]
        public IActionResult GetTourByLoaiTour([FromBody] SimpleReq req)
        {
            var res = new SingleRsp();
            res = tourSvc.Read(req.Keyword);
            return Ok(res);
        }

        [HttpPost("Get-All")]
        public IActionResult GetAll()
        {
            var res = new SingleRsp();
            res.Data = tourSvc.All;
            return Ok(res);
        }

        [HttpPut("Update")]
        public IActionResult Update([FromBody] TourReq tourReq)
        {
            var res = new SingleRsp();
            res = tourSvc.Update(tourReq);
            return Ok(res);
        }

        [HttpPost("Create")]
        public IActionResult Insert([FromBody] TourReq tourReq)
        {
            var res = new SingleRsp();
            res = tourSvc.Insert(tourReq);
            return Ok(res);
        }

        [HttpDelete("Delete")]
        public IActionResult Delete([FromBody] SimpleReq req)
        {
            var res = new SingleRsp();
            res = tourSvc.Delete(req.Id);
            return Ok(res);
        }

    }
}
