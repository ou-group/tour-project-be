﻿using System;
using System.Collections.Generic;

namespace DAL.Models;

public partial class NguoiDung
{
    public int Id { get; set; }

    public string? TaiKhoan { get; set; }

    public string? MatKhau { get; set; }

    public string? Ten { get; set; }

    public string? DiaChi { get; set; }

    public string? Sdt { get; set; }

    public string? Email { get; set; }

    public int? LoaiTaiKhoan { get; set; }

    public virtual ICollection<BinhLuan> BinhLuans { get; } = new List<BinhLuan>();

    public virtual ICollection<DatTour> DatTours { get; } = new List<DatTour>();
}
