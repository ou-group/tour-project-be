﻿using Common.DAL;
using Common.Rsp;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TourRep:GenericRep<QuanLyTourContext, Tour>
    {
        public override Tour Read(string key)
        {
            var res = All.FirstOrDefault(c => c.LoaiTourId.Contains(key));
            return res;
        }

        public SingleRsp Delete(int id)
        {
            var res = new SingleRsp();
            using (var context = new QuanLyTourContext())
            {
                using (var tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        var e = All.FirstOrDefault(s => s.Id == id);
                        var em = context.Tours.Remove(e);
                        context.SaveChanges();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        res.SetError(ex.StackTrace);
                    }
                }
            }
            return res;
        }

        public SingleRsp Update(Tour tour)
        {
            var res = new SingleRsp();
            using (var context = new QuanLyTourContext())
            {
                using (var tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        var em = All.SingleOrDefault(s => s.Id == tour.Id);
                        em = tour;
                        var e = context.Tours.Update(em);
                        context.SaveChanges();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        res.SetError(ex.StackTrace);
                    }
                }
            }
            return res;
        }

        public SingleRsp Insert(Tour tour)
        {
            var res = new SingleRsp();
            using (var context = new QuanLyTourContext())
            {
                using (var tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        var e = context.Tours.Add(tour);
                        context.SaveChanges();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        res.SetError(ex.StackTrace);
                    }
                }
            }
            return res;
        }
    }
}
